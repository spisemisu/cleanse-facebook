#!/usr/bin/env bash

clear

src="$(stack path --local-install-root)/bin"
tgt="./bin"

echo "### Clearing binary files:"
find $src -mindepth 1 -name "*" -delete -print
find $tgt -mindepth 1 -name "*" -delete -print
echo

echo "### Stack cleaning and building:" 
stack clean
#stack build --verbosity debug
stack build
echo

echo "### Copying binary to local $tgt:" 
if [ ! -d $tgt ]; then
  mkdir -p $tgt;
fi
cp -v $src/* $tgt/
echo

echo "### Clearing all .cabal files:" 
find . -name "*.cabal" -delete -print
echo

bin=$(ls $tgt) # We need to ls after binaries are created
echo "### Repoducible hashes:"
for f in $bin; do
    echo -e $(sha256sum $tgt/$f | cut -d " " -f 1): $f
done;
echo
