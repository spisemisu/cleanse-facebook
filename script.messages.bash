#!/usr/bin/env bash

clear

cd src

./CFB.hs messages \
    --usr="juane@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --recent \
    --tentative
#    --confirmed

#    --recent \
#    --pending \
#    --archived \
#    --other \
#    --spam \
