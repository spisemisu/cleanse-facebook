#!/usr/bin/env stack
{- stack
   --resolver lts-13.19
   --install-ghc
   script
   --ghc-options -Werror
   --ghc-options -Wall
   --package bytestring
   --package cmdargs
   --package data-default
   --package echo
   --package http-client
   --package http-client-tls
   --package http-types
   --package network
   --package tagsoup
   --package time
   --
-}

--------------------------------------------------------------------------------

{-
{-# LANGUAGE Safe               #-}

* CleanseFacebook.Api:
  -- Can't be safely imported! The module itself isn't safe

* CleanseFacebook.Cmd:
  -- Can't be safely imported! The module itself isn't safe

* CleanseFacebook.Eff:
  -- Can't be safely imported! The module itself isn't safe
-}

--------------------------------------------------------------------------------

module Main (main) where

--------------------------------------------------------------------------------

import qualified CleanseFacebook.Api as Logic
import qualified CleanseFacebook.Cmd as Command
import qualified CleanseFacebook.Dom as Domain
import qualified CleanseFacebook.Eff as Effects

--------------------------------------------------------------------------------

granulated
  ::
    ( Effects.ConsoleArgsM m
    , Effects.ConsoleOutM m
    , Effects.ConsoleInM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => m ()

main
  :: IO ()

--------------------------------------------------------------------------------

granulated =
  do
    Effects.putStr' "Password: "
    password  <- Effects.getPassword
    Effects.putStrLn' "********************************"

    manager   <- Effects.tlsManager
    arguments <- Command.arguments

    cookies   <- Logic.authentication manager password arguments

    case Logic.uniqueIdentifier cookies of

      Just identifier ->
        case arguments of

          Domain.LogList _ agent ->
            Logic.logList

            manager
            cookies

            identifier

            agent

          Domain.Cleanse _ agent operations log' years months method ->
            Logic.cleanse

            manager
            cookies

            identifier

            agent

            operations

            log'
            years
            months

            method

          Domain.Messages _ agent folder method ->
            Logic.messages

            manager
            cookies

            agent

            folder

            method

      Nothing ->
        Effects.putStrLn'
        $ "- Incorrect user credentials. Please check usr/pwd and try again."

main =
  granulated
