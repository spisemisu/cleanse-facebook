{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE Safe               #-}

--------------------------------------------------------------------------------

module CleanseFacebook.Dom
  ( Method
    ( Tentative
    , Confirmed
    )
  , Folder
    ( Recent
    , Pending
    , Other
    , Archived
    , SPAM
    )
  , Cleanse
    ( Cleanse
    , LogList
    , Messages
    , usr
    , agent
    , operation
    , log
    , year
    , month
    , method
    , folder
    )
  , Username
  , UserAgent
  , Operation
  , FilterLog
  , FilterYear
  , FilterMonth
  , username
  , useragent
  , operation'
  , filterlog
  , filteryear
  , filtermonth
  , mobileUri
  ) where

--------------------------------------------------------------------------------

import           Data.Data
  ( Data
  )
import           Data.Default
  ( Default
  , def
  )
import           Data.Typeable
  ( Typeable
  )
import           Prelude       hiding
  ( log
  )

--------------------------------------------------------------------------------

data Method
  = Tentative
  | Confirmed
  deriving (Data, Typeable, Show)

data Folder
  = Recent
  | Pending
  | Other
  | Archived
  | SPAM
  deriving (Data, Typeable, Show)

data Cleanse
  = Cleanse
  { usr       :: Username
  , agent     :: UserAgent
  , operation :: Operation
  , log       :: FilterLog
  , year      :: FilterYear
  , month     :: FilterMonth
  , method    :: Method
  }
  | LogList
  { usr   :: Username
  , agent :: UserAgent
  }
  | Messages
  { usr    :: Username
  , agent  :: UserAgent
  , folder :: Folder
  , method :: Method
  }
  deriving (Data, Typeable, Show)

newtype Username
  = Username { username :: String }
  deriving (Data, Typeable, Show)

newtype UserAgent
  = UserAgent { useragent :: String }
  deriving (Data, Typeable, Show)

newtype Operation
  = Operation { operation' :: [ String ] }
  deriving (Data, Typeable, Show)

newtype FilterLog
  = FilterLog { filterlog :: String }
  deriving (Data, Typeable, Show)
newtype FilterYear
  = FilterYear { filteryear :: [ Integer ] }
  deriving (Data, Typeable, Show)
newtype FilterMonth
  = FilterMonth { filtermonth :: [ Integer ] }
  deriving (Data, Typeable, Show)

--------------------------------------------------------------------------------

instance Default Method where
  def = Tentative

instance Default Folder where
  def = Recent

instance Default Cleanse where
  def =
    Cleanse
    def
    def
    def
    def def def
    def

instance Default Username where
  def = Username def

instance Default UserAgent where
  def = UserAgent def

instance Default Operation where
  def = Operation def

instance Default FilterLog where
  def = FilterLog def
instance Default FilterYear where
  def = FilterYear def
instance Default FilterMonth where
  def = FilterMonth def

--------------------------------------------------------------------------------

mobileUri
  :: String

--------------------------------------------------------------------------------

mobileUri =
  "https://@m.facebook.com"
