{-# LANGUAGE OverloadedStrings #-}
{-
{-# LANGUAGE Safe               #-}

* Network.HTTP.Client:
  -- Can't be safely imported! The module itself isn't safe

* CleanseFacebook.Eff:
  -- Can't be safely imported! The module itself isn't safe

-}

--------------------------------------------------------------------------------

module CleanseFacebook.Api
  ( authentication
  , cleanse
  , logList
  , uniqueIdentifier
  , messages
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Char8      as C8
import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Maybe
  ( catMaybes
  )
import           Data.String
  ( IsString
  )
import           Network.HTTP.Client
  ( Cookie (Cookie)
  , destroyCookieJar
  , evictExpiredCookies
  , insertCookiesIntoRequest
  , method
  , requestHeaders
  , responseBody
  , responseCookieJar
  , urlEncodedBody
  )
import           Network.HTTP.Client
  ( CookieJar
  , Manager
  , Response
  )
import           Network.HTTP.Types
  ( parseQuery
  )
import           Network.HTTP.Types.Method
  ( methodPost
  )
import           Prelude                    hiding
  ( log
  )
import           Text.HTML.TagSoup
  ( Tag (TagClose, TagOpen, TagText)
  , fromAttrib
  , parseTags
  , sections
  , (~==)
  )

--------------------------------------------------------------------------------

import qualified CleanseFacebook.Dom        as Domain
import qualified CleanseFacebook.Eff        as Effects

--------------------------------------------------------------------------------

cartProd
  :: [a]
  -> [b]
  -> [(a,b)]

loginUri
  :: String
messagesUri
  :: String
activityUri
  :: String
  -> String
  -> String
logoptionsUri
  :: String
  -> String

headers
  :: ( IsString b
     , IsString a
     )
  => b
  -> [(a, b)]

getWithBody
  ::
    ( Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> String
  -> String
  -> m (Response L8.ByteString)

getWithBody'
  ::
    ( Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> String
  -> [(C8.ByteString, C8.ByteString)]
  -> String
  -> m (Response L8.ByteString)

get
  ::
    ( Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> String
  -> String
  -> m (Response ())

--------------------------------------------------------------------------------

periodTags
  :: Foldable t
  => L8.ByteString
  -> t L8.ByteString
  -> [(L8.ByteString, L8.ByteString)]

action
  ::
    ( Foldable t
    , Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Method
  -> t (L8.ByteString, L8.ByteString)
  -> m ()

loop
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Operation
  -> Domain.Method
  -> [Char]
  -> m ()

months
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Operation
  -> L8.ByteString
  -> Domain.FilterMonth
  -> Domain.Method
  -> String
  -> m ()

activity
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Operation
  -> Domain.FilterYear
  -> Domain.FilterMonth
  -> Domain.Method
  -> String
  -> m ()

--------------------------------------------------------------------------------

msgTags
  :: L8.ByteString
  -> [ L8.ByteString ]

moreTags
  :: L8.ByteString
  -> [ L8.ByteString ]

formTags
  :: L8.ByteString
  -> [ (L8.ByteString, [ (L8.ByteString, L8.ByteString) ]) ]

delTags
  :: L8.ByteString
  -> [ L8.ByteString ]

msgLoop
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Method
  -> [ Char ]
  -> m ()

msgAux
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Method
  -> [[ Char ]]
  -> m ()

msgForm
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Method
  -> [(L8.ByteString, [(L8.ByteString, L8.ByteString)])]
  -> m ()

msgDel
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Method
  -> [[ Char ]]
  -> m ()

--------------------------------------------------------------------------------

authentication
  :: Effects.FacebookMobileWebM m
  => Manager
  -> String
  -> Domain.Cleanse
  -> m CookieJar

uniqueIdentifier
  :: CookieJar
  -> Maybe [Char]

--------------------------------------------------------------------------------

logList
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> String
  -> Domain.UserAgent
  -> m ()

cleanse
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> String
  -> Domain.UserAgent
  -> Domain.Operation
  -> Domain.FilterLog
  -> Domain.FilterYear
  -> Domain.FilterMonth
  -> Domain.Method
  -> m ()

messages
  ::
    ( Effects.ConsoleOutM m
    , Effects.DateTimeM m
    , Effects.FacebookMobileWebM m
    )
  => Manager
  -> CookieJar
  -> Domain.UserAgent
  -> Domain.Folder
  -> Domain.Method
  -> m ()

--------------------------------------------------------------------------------

cartProd xs ys =
  (,) <$> xs <*> ys

loginUri =
  "/" ++ "login.php"
messagesUri =
  "/" ++ "messages/?folder="
activityUri uid log =
  "/" ++ uid ++ "/" ++ "allactivity" ++ logFilter
  where logFilter = "?log_filter=" ++ log
logoptionsUri uid =
  "/" ++ "allactivity" ++ "/" ++ "options" ++ options
  where options = "?id=" ++ uid

headers agent =
  [ ( "User-Agent"
    , agent
    )
  , ( "Accept"
    , "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
    )
  , ( "Accept-Language"
    , "en-US,en;q=0.5"
    )
  , ( "Accept-Encoding"
    , "gzip, deflate"
    )
  , ( "Referer"
    , "https://m.facebook.com/"
    )
  , ( "AllowAutoRedirect", "false"
    )
  , ( "DNT", "1"
    )
  , ( "Connection"
    , "keep-alive"
    )
  , ( "Upgrade-Insecure-Requests"
    , "1"
    )
  , ( "Cache-Control"
    , "max-age=0"
    )
  ]

getWithBody manager cookies agent url =
  do
    timestamp <- Effects.getCurrentTime'
    request   <- Effects.parseRequest' $ url

    let (request',_) =
          insertCookiesIntoRequest
          (request { requestHeaders = headers $ C8.pack agent })
          (evictExpiredCookies cookies timestamp) timestamp

    response <- Effects.httpLbs' request' manager

    return response

getWithBody' manager cookies agent body url =
  do
    timestamp <- Effects.getCurrentTime'
    request   <- Effects.parseRequest' $ url

    let (request',_) =
          insertCookiesIntoRequest
          (
            urlEncodedBody body $
            request { requestHeaders = headers $ C8.pack agent }
          )
          (evictExpiredCookies cookies timestamp) timestamp

    response <- Effects.httpLbs' request' manager

    return response

get manager cookies agent url =
  do
    timestamp <- Effects.getCurrentTime'
    request   <- Effects.parseRequest' $ url

    let (request',_) =
          insertCookiesIntoRequest
          (request { requestHeaders = headers $ C8.pack agent })
          (evictExpiredCookies cookies timestamp) timestamp

    response <- Effects.httpNoBody' request' manager

    return response

periodTags body periods =
  catMaybes
  $ map f
  $ filter g
  $ sections (~== ("<div>" :: String))
  $ parseTags
  $ body

  where
    f ( TagOpen  "div" _
      : TagOpen  "a"   attrs
      : TagText        text
      : TagClose "a"
      : TagClose "div"
      : _
      ) =
      Just
      $
      ( text
      , head
        $ map snd
        $ filter (\(k,_) -> k == "href") attrs
      )
    f _ =
      Nothing

    g (t @ (TagOpen "div" _):_) =
      foldl (\a x -> a || (x `L8.isSuffixOf` fromAttrib "id" t)) False periods
    g _ =
      False

action manager cookies agent method' urls =
  mapM_
  (
    \(operation, url) ->
      do
        let contentId =
              case aux of
                [ ] -> "(No provided content identifier)"
                x:_ -> x
              where
                aux =
                  map C8.unpack
                  $ catMaybes
                  $ map snd
                  $ filter
                  (
                    \(k,_) ->
                      k == "timeline_token" ||
                      k == "story_identifier"
                  )
                  $ parseQuery
                  $ C8.pack
                  $ L8.unpack url
        let (operation', url') = (L8.unpack operation, L8.unpack url)

        Effects.putStrLn'
          $ "   - "
          ++ operation'
          ++ "\n       - "
          ++ contentId

        case method' of
          Domain.Tentative ->
            Effects.putStrLn' $ "       - (Operation not performed)"

          Domain.Confirmed ->
            do
              _ <-
                get manager cookies ( Domain.useragent agent ) url'

              return ()
  ) $ urls

loop manager cookies agent operations method' url =
  do
    response <-
      getWithBody manager cookies ( Domain.useragent agent ) url

    let body = responseBody response

    let tags =
          catMaybes
          $ map f
          $ sections (~== ("<a href>" :: String))
          $ parseTags
          $ body

          where
            f ( TagOpen  "a" attrs
              : TagText      text
              : TagClose "a"
              : _ ) =
              Just
              $
              ( text
              , head
                $ map snd
                $ filter (\(k,_) -> k == "href") attrs
              )
            f ( TagOpen  "a"   attrs
              : TagOpen  "div" _
              : TagOpen  "div" _
              : TagOpen  "h3"  _
              : TagText  text
              : TagClose "h3"
              : TagClose "div"
              : TagClose "div"
              : TagClose "a"
              : _ ) =
              Just
              $
              ( text
              , head
                $ map snd
                $ filter (\(k,_) -> k == "href") attrs
              )
            f _ =
              Nothing

    let operations' =
          filter
          (
            \(x,_) -> foldl (\a op -> a || (x == op)) False ops
          )
          $ tags
          where ops = map L8.pack $ Domain.operation' operations

    let more =
          map snd
          $ filter (\(x,_) -> "Load more from " `L8.isPrefixOf` x)
          $ tags

    action manager cookies agent method' operations'

    case more of
      next:_ ->
        do
          loop manager cookies agent operations method' $ L8.unpack next
      _ ->
        do
          return ()

    return ()

months manager cookies agent operations year months' method' url =
  do
    response <-
      getWithBody manager cookies ( Domain.useragent agent ) url

    let body     = responseBody response
    let months'' =
          periodTags body aux
          where
            aux =
              case Domain.filtermonth months' of
                [] ->
                  [ L8.pack $ "month_" ++ L8.unpack year ]
                ms ->
                  map (
                    \(y,m) -> L8.pack $ "month_" ++ L8.unpack y ++ "_" ++ show m
                  )
                  $ cartProd [ year ] ms

    mapM_
      (
        \(period,url') ->
          do
            Effects.putStrLn' $ "* " ++ L8.unpack period
            loop manager cookies agent operations method' $ L8.unpack url'
      ) $ months''

    return ()

activity manager cookies agent operations years months' method' url =
  do
    (year,_,_) <- Effects.getCurrentDate

    let currentYear =
          case Domain.filteryear years of
            [] ->
              Just year'
            ys ->
              if year `elem` ys then
                Just year'
              else
                Nothing
          where year' = L8.pack $ show year

    case currentYear of
      Nothing ->
        return ()
      Just year' ->
        do
          Effects.putStrLn' $ "# " ++ L8.unpack year'
          months manager cookies agent operations year' months' method' url

    response <-
      getWithBody manager cookies ( Domain.useragent agent ) url

    let body   = responseBody response

    let years' =
          periodTags body aux
          where
            aux =
              case Domain.filteryear years of
                [] ->
                  [ "year_" ]
                ys ->
                  map (\y -> L8.pack $ "year_" ++ show y) ys

    mapM_
      (
        \(year',url') ->
          do
            Effects.putStrLn' $ "# " ++ L8.unpack year'
            months manager cookies agent operations year' months' method'
              $ L8.unpack url'
      ) $ years'

    return ()

--------------------------------------------------------------------------------

msgTags body =
  concatMap id
  $ map f
  $ sections (~== ("<a href>" :: String))
  $ parseTags
  $ body
  where
    f ( TagOpen  "a" attrs
      : TagText      _
      : TagClose "a"
      : _ ) =
      filter ("/messages/read/" `L8.isPrefixOf`)
      $ map snd
      $ filter ((== "href") . fst) attrs
    f _ =
      [] -- Nothing

moreTags body =
  catMaybes
  $ map f
  $ sections (~== ("<a href>" :: String))
  $ parseTags
  $ body
  where
    f ( TagOpen  "a"    attrs
      : TagOpen  "span" _
      : TagText         _
      : TagClose "span"
      : TagClose "a"
      : _ ) =
      Just $ head $ map snd $ filter ((== "href") . fst) attrs
    f _ =
      Nothing

formTags body =
  filter (("/messages/action_redirect?" `L8.isPrefixOf`) . fst)
  $ catMaybes
  $ map f
  $ sections (~== ("<form>" :: String))
  $ parseTags
  $ body
  where
    f ( TagOpen  "form"  attrs
      : TagOpen  "input" foo
      : TagClose "input"
      : TagOpen  "input" bar
      : TagClose "input"
      : _ ) =
      Just
      (     head $ map snd $ filter ((== "action") . fst) attrs
      , [ ( head $ map snd $ filter ((== "name")   . fst) foo
          , head $ map snd $ filter ((== "value")  . fst) foo
          )
        , ( head $ map snd $ filter ((== "name")   . fst) bar
          , head $ map snd $ filter ((== "value")  . fst) bar
          )
        , ( "delete", "Delete" )
        ]
      )
    f _ =
      Nothing

delTags body =
  concatMap id
  $ map f
  $ sections (~== ("<a href>" :: String))
  $ parseTags
  $ body
  where
    f ( TagOpen  "a" attrs
      : TagText      _
      : TagClose "a"
      : _ ) =
      filter ("/messages/action/" `L8.isPrefixOf`)
      $ map snd
      $ filter ((== "href") . fst) attrs
    f _ =
      [] -- Nothing

msgLoop manager cookies agent method' url =
  do
    html <- getWithBody manager cookies ( Domain.useragent agent ) url
    let body = responseBody html
    msgAux manager cookies agent method' $ aux $ msgTags $ body
    mapM_ (msgLoop manager cookies agent method') $ aux $ moreTags body
    where
      aux = map L8.unpack

msgAux manager cookies agent method' =
  mapM_
  (
    \url ->
      do
        html <- getWithBody manager cookies ( Domain.useragent agent ) url
        let body = responseBody html
        msgForm manager cookies agent method' $ formTags body
  )

msgForm manager cookies agent method' =
  mapM_
    (
      \(url,input) ->
        do
          html <-
            getWithBody' manager cookies ( Domain.useragent agent )
            (
              map
              (
                \(k,v) -> (C8.pack . L8.unpack $ k, C8.pack . L8.unpack $ v)
              ) input
            )
            $ L8.unpack url
          let body = responseBody html
          msgDel manager cookies agent method' $ aux $ delTags body
    )
  where
    aux = map L8.unpack

msgDel manager cookies agent method' =
  mapM_
    (
      \(url) ->
        do
          let messageId =
                case aux of
                  [ ] -> "(No provided message identifier)"
                  x:_ -> x
                where
                  aux =
                    map C8.unpack
                    $ catMaybes
                    $ map snd
                    $ filter ((== "tids") . fst)
                    $ parseQuery
                    $ C8.pack
                    $ url
          Effects.putStrLn'
            $ "   - "
            ++ "Delete"
            ++ "\n       - "
            ++ messageId
          case method' of
            Domain.Tentative ->
              Effects.putStrLn' $ "       - (Operation not performed)"
            Domain.Confirmed ->
              do
                _ <- get manager cookies ( Domain.useragent agent ) url
                return ()
    )

--------------------------------------------------------------------------------

authentication manager pwd arguments =
  do
    request <- Effects.parseRequest' loginUri

    let request' =
          urlEncodedBody body
          $ request
          { method = methodPost
          , requestHeaders = (headers agent)
          }

    response <- Effects.httpLbs' request' manager

    return $ responseCookieJar response

    where
      body =
        [ ( "email", usr)
        , ( "pass",  C8.pack pwd)
        , ( "login", "Log+In")
        ]
      (usr,agent) =
        case arguments of
          Domain.LogList usr' agent' ->
            ( C8.pack $ Domain.username  usr'
            , C8.pack $ Domain.useragent agent'
            )
          Domain.Cleanse usr' agent' _ _ _ _ _  ->
            ( C8.pack $ Domain.username  usr'
            , C8.pack $ Domain.useragent agent'
            )
          Domain.Messages usr' agent' _ _  ->
            ( C8.pack $ Domain.username  usr'
            , C8.pack $ Domain.useragent agent'
            )

uniqueIdentifier cookies =
  cuser
  $ map
  (
    \(Cookie _ value _ _ _ _ _ _ _ _ _) -> value
  )
  $ filter
  (
    \(Cookie name _ _ _ _ _ _ _ _ _ _) -> name == "c_user"
  )
  $ destroyCookieJar cookies
  where
    cuser (uid:_) = Just $ C8.unpack uid
    cuser _______ = Nothing

--------------------------------------------------------------------------------

logList manager cookies uid agent =
  do
    response <-
      getWithBody
      manager
      cookies
      ( Domain.useragent agent )
      $ logoptionsUri uid

    let body = responseBody response

    let tags =
          catMaybes
          $ map f
          $ sections (~== ("<a href>" :: String))
          $ parseTags
          $ body

          where
            f ( TagOpen  "a" attrs
              : TagText      text
              : TagClose "a"
              : _
              ) =
              Just
              $
              ( text
              , head
                $ map snd
                $ filter (\(k,_) -> k == "href") attrs
              )
            f _ =
              Nothing

    let list =
          map
          (
            \(name,url) ->
              "- " ++ (L8.unpack name) ++
              ": " ++ (takeWhile ((/=) '&') $ drop n $ L8.unpack url)
          )
          $ filter
          (
            \(_,x) -> (L8.pack p) `L8.isPrefixOf` x
          )
          $ tags
          where
            p = "/" ++ uid ++ "/allactivity?log_filter="
            n = length p

    mapM_ Effects.putStrLn' list

--------------------------------------------------------------------------------

cleanse manager cookies uid agent operations log years months' method' =
  activity manager cookies agent operations years months' method' url
  where
    url = activityUri uid $ Domain.filterlog log

--------------------------------------------------------------------------------

messages manager cookies agent folder method' =
  do
    Effects.putStrLn' msg
    msgLoop manager cookies agent method' url
      where
        msg = "# " ++ (show folder) ++ " messages:"
        url =
          messagesUri ++
          (
            case folder of
              Domain.Recent   -> ""
              Domain.Pending  -> "pending"
              Domain.Other    -> "other"
              Domain.Archived -> "action:archived"
              Domain.SPAM     -> "spam"
          )
