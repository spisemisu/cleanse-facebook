{-# LANGUAGE DeriveDataTypeable #-}
{-
{-# LANGUAGE Safe               #-}

* System.Console.CmdArgs:
  -- Can't be safely imported! The module itself isn't safe

* CleanseFacebook.Eff:
  -- Can't be safely imported! The module itself isn't safe

-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

--------------------------------------------------------------------------------

module CleanseFacebook.Cmd
  ( arguments
  ) where

--------------------------------------------------------------------------------

import           Data.Data
  ( Data
  )
import qualified Data.Default           as DD
import           Prelude                hiding
  ( log
  )
import           System.Console.CmdArgs
  ( Default
  , def
  , enum
  , help
  , modes
  , program
  , summary
  , (&=)
  )

--------------------------------------------------------------------------------

import qualified CleanseFacebook.Dom    as Domain
import qualified CleanseFacebook.Eff    as Effects

--------------------------------------------------------------------------------

instance Default Domain.Method where
  def = DD.def

instance Default Domain.Folder where
  def = DD.def

instance Default Domain.Cleanse where
  def = DD.def

instance Default Domain.Username where
  def = DD.def

instance Default Domain.UserAgent where
  def = DD.def

instance Default Domain.Operation where
  def = DD.def

instance Default Domain.FilterLog where
  def = DD.def
instance Default Domain.FilterYear where
  def = DD.def
instance Default Domain.FilterMonth where
  def = DD.def

--------------------------------------------------------------------------------

usr
  :: Data val
  => val
  -> val
agent
  :: Data val
  => val
  -> val
operation
  :: Data val
  => val
  -> val
log
  :: Data val
  => val
  -> val
year
  :: Data val
  => val
  -> val
month
  :: Data val
  => val
  -> val

loglist
  :: Domain.Cleanse
cleanse
  :: Domain.Cleanse
messages
  :: Domain.Cleanse

arguments
  :: Effects.ConsoleArgsM m
  => m Domain.Cleanse

--------------------------------------------------------------------------------

usr x =
  x
  &= help "Username/e-mail"

agent x =
  x
  &= help "Browser User-Agent header"

operation x =
  x
  &= help
  "Assign operations to be performed.\n\
  \> Example: ./CFB.hs               ... -o=\"Delete\" -o=\"Unlike\"\n\
  \> Example: ./bin/cleanse-facebook ... -o=\"Delete\" -o=\"Unlike\""

log x =
  x
  &= help
  "Optional: Filter by log.\n\
  \> Example: ./CFB.hs               ... -l=likes\n\
  \> Example: ./bin/cleanse-facebook ... -l=likes"

year x =
  x
  &= help
  "Optional: Filter by years.\n\
  \> Example: ./CFB.hs               ... --year=2011 --year=2017\n\
  \> Example: ./bin/cleanse-facebook ... --year=2011 --year=2017"

month x =
  x
  &= help
  "Optional: Filter by months.\n\
  \> Example: ./CFB.hs               ... --month=3 --month=11\n\
  \> Example: ./bin/cleanse-facebook ... --month=3 --month=11"

loglist =
  Domain.LogList

  ( usr def )

  ( agent def )
  &= help
  "List of values to be used to filter the Activity Log.\n\
  \> Example: ./CFB.hs               cleanse ... -l=likes ... -o=\"Unlike\"\n\
  \> Example: ./bin/cleanse-facebook cleanse ... -l=likes ... -o=\"Unlike\""

cleanse =
  Domain.Cleanse

  ( usr def )

  ( agent def )

  ( operation def )

  ( log   def )
  ( year  def )
  ( month def )

  ( enum
    [ Domain.Tentative
      &= help "Doesn't perform any cleansing (default)"
    , Domain.Confirmed
      &= help "Does perform the cleansing"
    ]
  )
  &= help
  "Cleanse data from Facebook based on diverse criterias.\n\
  \> Example: ./CFB.hs               cleanse ... -o=\"Unlike\" -l=likes -y=2017\n\
  \> Example: ./bin/cleanse-facebook cleanse ... -o=\"Unlike\" -l=likes -y=2017"

messages =
  Domain.Messages

  ( usr def )

  ( agent def )

  ( enum
    [ Domain.Recent
      &= help "All friends messages (default)"
    , Domain.Pending
      &= help "Message request from non-friends"
    , Domain.Archived
      &= help "Archived message threads"
    , Domain.Other
      &= help "Not so obvious SPAM messages"
    , Domain.SPAM
      &= help "Obvious SPAM messages"
    ]
  )

  ( enum
    [ Domain.Tentative
      &= help "Doesn't perform any cleansing (default)"
    , Domain.Confirmed
      &= help "Does perform the cleansing"
    ]
  )
  &= help
  "Cleanse message from Facebook based on folder type.\n\
  \> Example: ./CFB.hs               messages ... --recent --tentative\n\
  \> Example: ./bin/cleanse-facebook messages ... --recent --tentative"

arguments =
  do
    Effects.cmdArgs' $ modes [ loglist, cleanse, messages ]
      &= program "./CFB.hs or ./bin/cleanse-facebook"
      &= summary "Cleanse Facebook v0.11"
