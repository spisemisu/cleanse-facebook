{-
{-# LANGUAGE Safe               #-}

* Network.HTTP.Client:
  -- Can't be safely imported! The module itself isn't safe

* Network.HTTP.Client.TLS:
  -- Can't be safely imported! The module itself isn't safe

* System.Console.CmdArgs:
  -- Can't be safely imported! The module itself isn't safe

-}

--------------------------------------------------------------------------------

module CleanseFacebook.Eff
  ( ConsoleArgsM
  , ConsoleInM
  , ConsoleOutM
  , DateTimeM
  , FacebookMobileWebM
  , cmdArgs'
  , getCurrentDate
  , getCurrentTime'
  , getPassword
  , httpLbs'
  , httpNoBody'
  , parseRequest'
  , putStr'
  , putStrLn'
  , tlsManager
  ) where

--------------------------------------------------------------------------------

import qualified Data.ByteString.Lazy.Char8 as L8
import           Data.Data
  ( Data
  )
import           Data.Time
  ( UTCTime
  )
import           Data.Time.Calendar
  ( toGregorian
  )
import           Data.Time.Clock
  ( getCurrentTime
  , utctDay
  )
import           Network.HTTP.Client
  ( Manager
  , Request
  , Response
  , httpLbs
  , httpNoBody
  , newManager
  , parseRequest
  )
import           Network.HTTP.Client.TLS
  ( tlsManagerSettings
  )
import           System.Console.CmdArgs
  ( cmdArgs
  )
import           System.IO
  ( hFlush
  , stdout
  )
import           System.IO.Echo
  ( withoutInputEcho
  )

--------------------------------------------------------------------------------

import qualified CleanseFacebook.Dom        as Domain

--------------------------------------------------------------------------------

class Monad m => DateTimeM m where
  getCurrentTime'
    :: m UTCTime
  getCurrentDate
    :: m (Integer,Int,Int)

class Monad m => ConsoleInM m where
  getPassword
    :: m String

class Monad m => ConsoleOutM m where
  putStr'
    :: String
    -> m ()
  putStrLn'
    :: String
    -> m ()

class Monad m => ConsoleArgsM m where
  cmdArgs'
    :: Data a
    => a
    -> m a

class Monad m => FacebookMobileWebM m where
  parseRequest'
    :: String
    -> m Request
  httpLbs'
    :: Request
    -> Manager
    -> m (Response L8.ByteString)
  httpNoBody'
    :: Request
    -> Manager
    -> m (Response ())
  tlsManager
    :: m Manager

--------------------------------------------------------------------------------

instance DateTimeM IO where
  getCurrentTime'
    = getCurrentTime

  getCurrentDate
    = getCurrentTime >>= return . toGregorian . utctDay

instance ConsoleInM IO where
  getPassword
    = withoutInputEcho getLine

instance ConsoleOutM IO where
  putStr' x =
    putStr x >> hFlush stdout

  putStrLn' x =
    putStrLn x >> hFlush stdout

instance ConsoleArgsM IO where
  cmdArgs' = cmdArgs

instance FacebookMobileWebM IO where
  parseRequest' relativeUrl =
    parseRequest $ Domain.mobileUri ++ relativeUrl

  httpLbs' =
    httpLbs

  httpNoBody' =
    httpNoBody

  tlsManager =
    newManager tlsManagerSettings
