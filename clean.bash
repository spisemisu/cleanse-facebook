#!/usr/bin/env bash

bin="./bin"

clear

stack clean

find $bin -mindepth 1 -name "*"           -delete -print
find .                -name "*.cabal"     -delete -print
find .                -name ".stack-work" -type d -exec rm -rv "{}" \;
