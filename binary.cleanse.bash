#!/usr/bin/env bash

clear

./bin/cleanse-facebook cleanse \
    --usr="juan@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --operation="Delete" --operation="Unvote" \
    --operation="Unlike" --operation="Remove Reaction" \
    --year=2019 \
    --month=1 --month=2 --month=3 --month=4 \
    --tentative
#    --confirmed

#    --log="cluster_116" \
