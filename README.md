Haskell Script
==============

Haskell Script to Remove Facebook Content that was prepared for the MF#K Meetup:

* [Talk] MF#K: How to remove Facebook content programmatically (Round II)
    - https://www.meetup.com/MoedegruppeFunktionelleKoebenhavnere/events/250240206/

### Usage

#### Script

```
./CFB.hs loglist \
    --usr="juana@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)"
```
or
```
./CFB.hs cleanse \
    --usr="juan@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --operation="Delete" --operation="Unvote" \
    --operation="Unlike" --operation="Remove Reaction" \
    --year=2019 \
    --month=1 --month=2 --month=3 --month=4 \
    --tentative
```
or

```
./CFB.hs messages \
    --usr="juane@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --recent \
    --tentative
```

#### Binary

```
./bin/cleanse-facebook loglist \
    --usr="juana@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)"
```
or
```
./bin/cleanse-facebook cleanse \
    --usr="juan@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --operation="Delete" --operation="Unvote" \
    --operation="Unlike" --operation="Remove Reaction" \
    --year=2019 \
    --month=1 --month=2 --month=3 --month=4 \
    --tentative
```
or

```
./bin/cleanse-facebook messages \
    --usr="juane@de.la.cierva.co.es" \
    --agent="Links (2.14; Linux x86_64; text)" \
    --recent \
    --tentative
```

For more information, type`./CFB.hs or ./bin/cleanse-facebook --help` to get the
following text:

```
Cleanse Facebook v0.11

./CFB.hs or ./bin/cleanse-facebook [COMMAND] ... [OPTIONS]

Common flags:
  -u --usr=ITEM         Username/e-mail
  -? --help             Display help message
  -V --version          Print version information
     --numeric-version  Print just the version number

./CFB.hs or ./bin/cleanse-facebook loglist [OPTIONS]
  List of values to be used to filter the Activity Log.
  > Example: ./CFB.hs               cleanse ... -l=likes ... -o="Unlike"
  > Example: ./bin/cleanse-facebook cleanse ... -l=likes ... -o="Unlike"

  -a --agent=ITEM       Browser User-Agent header

./CFB.hs or ./bin/cleanse-facebook cleanse [OPTIONS]
  Cleanse data from Facebook based on diverse criterias.
  > Example: ./CFB.hs               cleanse ... -o="Unlike" -l=likes -y=2017
  > Example: ./bin/cleanse-facebook cleanse ... -o="Unlike" -l=likes -y=2017

  -a --agent=ITEM       Browser User-Agent header
  -o --operation=ITEM   Assign operations to be performed.
                        > Example: ./CFB.hs               ... -o="Delete"
                        -o="Unlike"
                        > Example: ./bin/cleanse-facebook ... -o="Delete"
                        -o="Unlike"
  -l --log=ITEM         Optional: Filter by log.
                        > Example: ./CFB.hs               ... -l=likes
                        > Example: ./bin/cleanse-facebook ... -l=likes
  -y --year=INT         Optional: Filter by years.
                        > Example: ./CFB.hs               ... --year=2011
                        --year=2017
                        > Example: ./bin/cleanse-facebook ... --year=2011
                        --year=2017
  -m --month=INT        Optional: Filter by months.
                        > Example: ./CFB.hs               ... --month=3
                        --month=11
                        > Example: ./bin/cleanse-facebook ... --month=3
                        --month=11
  -t --tentative        Doesn't perform any cleansing (default)
  -c --confirmed        Does perform the cleansing

./CFB.hs or ./bin/cleanse-facebook messages [OPTIONS]
  Cleanse message from Facebook based on folder type.
  > Example: ./CFB.hs               messages ... --recent --tentative
  > Example: ./bin/cleanse-facebook messages ... --recent --tentative

     --agent=ITEM       Browser User-Agent header
  -r --recent           All friends messages (default)
  -p --pending          Message request from non-friends
     --archived         Archived message threads
  -o --other            Not so obvious SPAM messages
  -s --spam             Obvious SPAM messages
  -t --tentative        Doesn't perform any cleansing (default)
  -c --confirmed        Does perform the cleansing
```

### Requirements

In order to run the script, `The Haskell Tool Stack` must be installed:

* https://docs.haskellstack.org/en/stable/install_and_upgrade/
